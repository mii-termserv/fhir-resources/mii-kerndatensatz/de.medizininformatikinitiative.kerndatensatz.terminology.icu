## Terminologie MII KDS Erweiterungsmodul ICU

Dieses Paket enthält die Terminologie aus dem Erweiterungsmodul ICU des Kerndatensatzes der MII.

Diese Ressourcen wurden umpaketiert aus dem Ursprungspaket: https://simplifier.net/packages/de.medizininformatikinitiative.kerndatensatz.icu/1.0.0.

Sobald neue Versionen veröffentlicht werden, werden diese auf anderen Git-Branches verfügbar gemacht. Sie sehen aktuell den Stand vom Branch **1.0.0**.
